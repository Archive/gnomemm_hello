#include <gtkmm/label.h>
#include <gtkmm/button.h>
#include <gtkmm/frame.h>
#include <gtkmm/main.h>
#include <libgnomeuimm/app.h>
#include <libgnomeuimm/appbar.h>
#include <libgnomeuimm/about.h>
#include <vector>
#include <list>


class Hello_App;

class AppSet : public sigc::trackable
{
public:
  friend class Hello_App;

  virtual void add_app(Hello_App* pApp);
  virtual void close_all();
  virtual void on_app_hide(Hello_App* pApp);

protected:
  std::list<Hello_App*> instances_;
};

static AppSet appset;


class Hello_App : public Gnome::UI::App
{
public:
  Hello_App(const Glib::ustring& message, const std::vector<Glib::ustring>& greet);
  Hello_App(const Glib::ustring& message); // used by new_app_cb()
  virtual ~Hello_App();

protected:
  void init();

  void install_menus_and_toolbar();

  // Signal Handlers:
  virtual void on_menu_file_new();
  virtual void on_menu_file_close();
  virtual void on_menu_help_about();
  virtual void on_menu_file_exit();

  virtual void on_button_clicked();
  void nothing_cb();

  virtual void on_about_hide();

  
  // widgets
  Gtk::Button button_;
  Gtk::Label label_;
  Gnome::UI::AppBar status_;
  Gtk::Frame frame_;

  Gnome::UI::About* about_;
};

